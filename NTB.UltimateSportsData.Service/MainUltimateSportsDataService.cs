﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;
using NTB.UltimateSportsData.Components;

namespace NTB.UltimateSportsData.Service
{
    public partial class MainUltimateSportsDataService : ServiceBase
    {
        /// <summary>
        /// The _logger.
        /// </summary>
        private static ILog _logger = LogManager.GetLogger(typeof(MainUltimateSportsDataService));

        /// <summary>
        /// The service.
        /// </summary>
        protected MainServiceComponent Service = null;

        public MainUltimateSportsDataService()
        {
            // log4net.Config.XmlConfigurator.Configure();
            ThreadContext.Properties["JOBNAME"] = "NTB.UltimateSportsData.Service";
            log4net.Config.XmlConfigurator.Configure();

            _logger = LogManager.GetLogger(typeof(MainUltimateSportsDataService));

            _logger.Info("In MainUltimateSportsDataService - starting up");

            // Creating the new MainServiceComponent
            Service = new MainServiceComponent();

            // Initialising this main component
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                _logger.Info("NTB UltimateSportsData Service starting...");
                _startupConfigTimer.Start();
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTB UltimateSportsData Service DID NOT START properly - Terminating!", ex);
                throw;
            }
        }

        protected override void OnStop()
        {
            try
            {
                _logger.Info("Stopping service");
                Service.Stop();

                _startupConfigTimer.Enabled = false;
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTB UltimateSportsData Service did not stop properly - Terminating!", ex);
                throw;
            }
        }

        private void _startupConfigTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _startupConfigTimer.Stop();

            try
            {
                Service.Configure();
                Service.Start();
                _logger.Info("MainSportsDataService Started!");
            }
            catch (Exception ex)
            {
                _logger.Fatal("MainSportsDataService DID NOT START properly - TERMINATING!", ex);

                throw;
            }
        }
    }
}
