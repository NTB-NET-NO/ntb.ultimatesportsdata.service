﻿using System;
using System.Diagnostics;
using System.ServiceProcess;

namespace NTB.UltimateSportsData.Service
{
    static class ServiceStart
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            try
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new MainUltimateSportsDataService()
                };
                ServiceBase.Run(ServicesToRun);
            }
            catch (Exception exception)
            {
                EventLog appLog = new EventLog {Source = "NTB.UltimateSportsData.Service"};
                appLog.WriteEntry(exception.Message);
                appLog.WriteEntry(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    appLog.WriteEntry(exception.InnerException.Message);
                    appLog.WriteEntry(exception.InnerException.StackTrace);
    
                }

            }
        }
    }
}
