﻿using System;
using System.Windows.Forms;
using log4net;
using NTB.UltimateSportsData.Components;

namespace NTB.UltimateSportsData.Service
{
    public partial class DebugForm : Form
    {
        /// <summary>
        /// The actual service object
        /// </summary>
        /// <remarks>Does the actuall work</remarks>
        protected MainServiceComponent Service = null;

        /// <summary>
        /// Creating the logger object so we can log stuff
        /// </summary>
        private static ILog _logger = LogManager.GetLogger(typeof(DebugForm));

        public DebugForm()
        {
            ThreadContext.Properties["JOBNAME"] = "UltimateSportsData-Debug";
            log4net.Config.XmlConfigurator.Configure();
            _logger = LogManager.GetLogger(typeof(DebugForm));

            this.Service = new MainServiceComponent();

            InitializeComponent();
        }

        private void DebugForm_Load(object sender, EventArgs e)
        {
            try
            {
                _logger.Info("NTB Ultimate SportsData Service DEBUGMODE starting...");
                _startupConfigTimer.Start();
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTB Ultimate SportsData Service DEBUGMODE DID NOT START properly", ex);
            }
        }

        private void DebugForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                this.Service.Stop();
                _logger.Info("NTB Ultimate SportsData Service DEBUGMODE stopped");
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTB Ultimate SportsData Service DEBUGMODE DID NOT STOP properly", ex);
            }
        }

        private void _startupConfigTimer_Tick(object sender, EventArgs e)
        {
            // Kill the timer
            _startupConfigTimer.Stop();

            // And configure
            try
            {
                Service.Configure();
                Service.Start();
                _logger.Info("NTB Ultimate SportsData Service DEBUGMODE started");
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTB Ultimate SportsData Service DEBUGMODE DID NOT START properly", ex);
            }
        }
    }
}
