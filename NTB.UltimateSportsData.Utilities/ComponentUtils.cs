﻿// Adding support for log4net
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using log4net;

namespace NTB.UltimateSportsData.Utilities
{
    public static class Utilities
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Utilities));

        

        /// <summary>
        /// Initializes the <see cref="Utilities"/> class.
        /// </summary>
        static Utilities()
        {
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();

            //Load watermark cache
            //iptcSequenceFile = ConfigurationManager.AppSettings["IPTCSequenceCache"];
            //if (!String.IsNullOrEmpty(iptcSequenceFile))
            //{
            //    Exception ex;
            //    IPTCSequenceCache.LoadFromFile(iptcSequenceFile, out iptcSequenceCache, out ex);
            //    if (ex != null)
            //    {
            //        logger.WarnFormat("Utilities::Utilities() Failed to load IPTCSequence cache {0}: {1}", iptcSequenceFile, ex.Message);
            //        iptcSequenceCache = new IPTCSequenceCache();
            //    }
            //    logger.InfoFormat("Utilities::Utilities() IPTCSequence cache {0} contains {1} items", iptcSequenceFile, iptcSequenceCache.IPTCSequence.Count);
            //}
        }

        public static string ToDebugString<TKey, TValue>(this IDictionary<TKey, TValue> dictionary)
        {
            return string.Join(",", dictionary.Select(kv => kv.Key.ToString() + "=" + kv.Value.ToString()).ToArray());
        }

        public static string DictionaryValuesToString<TKey, TValue>(this IDictionary<TKey, TValue> dictionary)
        {
            return string.Join(",", dictionary.Select(kv => kv.Value.ToString()).ToArray());
        }

        /// <summary>
        /// Calculate the next time a <c>Scheduled</c> job should run
        /// </summary>
        /// <param name="schedule">The configured schedule to parse.</param>
        /// <returns>
        /// A TimeSpan that indicates how long is until the next Scheduled run.
        /// </returns>
        /// <remarks>
        /// The function is used to calculate run times for <c>Scheduled</c> jobs. It takes a time definition, which is a
        /// formatted time string, and tries to parse it as a time of day. If unsuccessfull 01:00 is used. A TimeSpan between now and the
        /// time of day parsed time of day is calculcated. It is used directly as a polling delay to ensure that a job is executed at the given time.
        /// </remarks>
        public static TimeSpan GetScheduleInterval(String schedule)
        {
            //Vars
            DateTime scheduledDateTime;
            DateTime currentDateTime = DateTime.Now;

            try
            {
                //Try to parse the time given
                scheduledDateTime = DateTime.Parse(schedule, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                //Default to 01:00
                Logger.Warn("ComponentUtils::SetScheduleInterval() failed to parse schedule string: " + schedule, ex);
                scheduledDateTime = DateTime.Parse("01:00");
            }

            // Determine next runtime
            DateTime next = new DateTime(currentDateTime.Year, currentDateTime.Month, currentDateTime.Day, scheduledDateTime.Hour, scheduledDateTime.Minute, 0);

            // Add a day if we passed for today
            if (scheduledDateTime.Hour < currentDateTime.Hour ||
                 (scheduledDateTime.Hour == currentDateTime.Hour && scheduledDateTime.Minute <= currentDateTime.Minute))
            {
                next = next.AddDays(1);
            }

            //Calculated actuall interval
            TimeSpan interval = next - currentDateTime;
            Logger.DebugFormat("ComponentUtils::GetScheduleInterval() calculated next scheduled poll: {0}", next.ToString("yyyy-MM-dd HH:mm:ss"));
            Logger.DebugFormat("ComponentUtils::GetScheduleInterval() interval milliseconds: {0}", interval.TotalMilliseconds);
            return interval;
        }

        public static string Utf8Decode(string content)
        {
            //byte[] bytes = Encoding.Default.GetBytes(content);
            //string encodedString = Encoding.UTF8.GetString(bytes);

            //return encodedString.Trim();

            byte[] bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(content);
            return Encoding.UTF8.GetString(bytes);

        }
    }
}
