﻿namespace NTB.UltimateSportsData.Utilities
{
    // Consider doing this class static
    public class Customer
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public int RemoteCustomerId { get; set; }
    }
}
