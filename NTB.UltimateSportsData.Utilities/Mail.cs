﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using log4net;

namespace NTB.UltimateSportsData.Utilities
{
    public static class Mail
    {
        // This class shall be used to send out mail messages when there is an exception

        /// <summary>
        /// Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(Mail));

        public static void SendException(Exception exception)
        {
            string fromAddress = ConfigurationManager.AppSettings["SMTPFromAddress"];
            string toAddress = ConfigurationManager.AppSettings["SMTPToAddress"];
            string smtpServer = ConfigurationManager.AppSettings["SMTPServer"];
            List<string> addresses = new List<string>();


            if (!toAddress.Contains(";"))
            {
                addresses.Add(toAddress);

            }
            else
            {
                string[] toAddresses = toAddress.Split(';');

                addresses.AddRange(toAddresses);
            }

            foreach (MailMessage message in addresses.Select(address => new MailMessage(fromAddress, address)))
            {
                string mailMessage = "An error has occured in SportsData Service: " + exception.Message +
                                     Environment.NewLine;
                mailMessage += "Stack Trace: " + exception.StackTrace + Environment.NewLine;

                if (exception.InnerException != null)
                {
                    mailMessage += "Inner Exception: " + exception.InnerException.Message + Environment.NewLine;
                    mailMessage += exception.InnerException.StackTrace + Environment.NewLine;
                }
                message.Subject = "Error in SportsData Service";
                message.Body = mailMessage;

                SmtpClient client = new SmtpClient(smtpServer);
                try
                {
                    client.Send(message);
                }
                catch (Exception sendException)
                {
                    Logger.Error(sendException.Message);
                    Logger.Error(sendException.StackTrace);

                }
            }

        }
    }
}
