﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using log4net;
using NTB.UltimateSportsData.Classes.Classes;


namespace NTB.UltimateSportsData.Components.ApiClients
{
    internal class ApiClient
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof (ApiClient));

        static ApiClient()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();
        }

        private Api _api { get; set; }

        public ApiClient(Api api)
        {
            _api = api;
        }

        public string GetResult(int eventId, int records, int distance)
        {
            string responseBody = string.Empty;
            Logger.Debug("Starting web client");
            using (var client = new HttpClient())
            {
                try
                {
                    var webUrl = _api.Url + "?eventid=" + eventId + "&type=xml"
                                 + "&method=timingpointstandings"
                                 + "&time=Finish"
                                 + "&apiuser=" + _api.User
                                 + "&distance=" + distance
                                 + "&records=" + records
                                 + "&apikey=" + _api.Token;
                    Logger.Debug("Created URL: " + webUrl);

                    client.MaxResponseContentBufferSize = 2147483647;

                    client.DefaultRequestHeaders.Add("user-agent",
                        "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");

                    Logger.Info("Getting content async");
                    var response = client.GetAsync(webUrl).Result;

                    
                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = response.Content;

                        responseBody = responseContent.ReadAsStringAsync().Result;
                    }

                    Logger.Debug("Returning body containing " + responseBody.Length + "Characters");
                    
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    
                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }

                    // Making sure the string is empty because of this error
                    responseBody = string.Empty;
                }
                return responseBody;
            }

        }
    }
}
