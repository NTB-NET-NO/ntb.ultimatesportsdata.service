﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace NTB.UltimateSportsData.Components
{
    class FileCleaner
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(FileCleaner));
        // �?

        public FileCleaner()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();
        }

        public string CleanFile(string content)
        {
            int counter = 0;
            string line = string.Empty;
            var cleanFileDictionary = ConfigurationManager.AppSettings["CleanFileDictionary"];
            StreamReader fileReader = new StreamReader(cleanFileDictionary);

            while ((line = fileReader.ReadLine()) != null)
            {
                counter++;
                string[] lineContent = line.Split('=');
                string find = lineContent[0];
                string replace = lineContent[1];
                content = content.Replace(find, replace);
            }

            fileReader.Close();
            
            return content;
        }
    }
}
