﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using log4net;
using NTB.UltimateSportsData.Classes.Enums;
using NTB.UltimateSportsData.Components.Interfaces;
using NTB.UltimateSportsData.Utilities;


namespace NTB.UltimateSportsData.Components
{
    public partial class MainServiceComponent : Component
    {
        #region Instance variables and control data

        /// <summary>
        /// Name of the file that contains the job configuration set
        /// </summary>
        /// <remarks>The name of the job config XML file is stored here.</remarks>
        protected string ConfigSet = string.Empty;

        /// <summary>
        /// Enable watching the config set for changes and reconfigure at runtime
        /// </summary>
        /// <remarks>config setting that defines if we are wathcing the config set for changes/auto reconfigure or not.</remarks>
        protected bool WatchConfigSet = false;

        /// <summary>
        /// List of currently Configured jobs
        /// </summary>
        /// <remarks>Internal Dictionary to keep track of running worker jobs.</remarks>
        protected Dictionary<string, IBaseUltimateSportsDataInterface> JobInstances =
            new Dictionary<string, IBaseUltimateSportsDataInterface>();

        /// <summary>
        /// Notification handler service host
        /// </summary>
        /// <remarks>
        /// This si the actuall service host that controls the notification object <see>
        ///                                                                            <cref>ewsNotify</cref>
        ///                                                                        </see>
        /// </remarks>
        protected ServiceHost ServiceHost = null;

        #endregion

        /// <summary>
        /// The _logger.
        /// </summary>
        private static ILog _logger = LogManager.GetLogger(typeof(MainServiceComponent));

        static MainServiceComponent()
        {
            // Set up _logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }

            _logger = LogManager.GetLogger(typeof(MainServiceComponent));

            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = "MainServiceComponent";
            }
        }

        public MainServiceComponent()
        {
            
            InitializeComponent();
        }

        public MainServiceComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public void Configure()
        {
            // NDC.Push("CONFIG");
            ThreadContext.Stacks["NDC"].Push("CONFIG");

            // Checking if a directory exists or not
            DirectoryInfo di = new DirectoryInfo(ConfigurationManager.AppSettings["OutPath"]);

            if (!di.Exists)
            {
                di.Create();
            }

            // Read params 
            ConfigSet = ConfigurationManager.AppSettings["ConfigurationSet"];
            WatchConfigSet = Convert.ToBoolean(ConfigurationManager.AppSettings["WatchConfiguration"]);

            // Logging
            _logger.InfoFormat("{0} : {1}", "ConfigurationSet", ConfigSet);
            _logger.InfoFormat("{0} : {1}", "WatchConfiguration", WatchConfigSet);

            // Load configuration set
            XmlDocument config = new XmlDocument();
            config.Load(ConfigSet);

            // Setting up the watcher
            configFileWatcher.Path = Path.GetDirectoryName(ConfigSet);
            configFileWatcher.Filter = Path.GetFileName(ConfigSet);

            // Clearing all jobInstances before we populate them again. 
            JobInstances.Clear();

            // Creating NFF Components
            XmlNodeList nodes =
                config.SelectNodes("/ComponentConfiguration/SportsDataComponents/SportsDataComponent[@Enabled='True']");
            if (nodes != null)
            {
                _logger.InfoFormat("SportsDataComponent job instances found: {0}", nodes.Count);

                bool checkDatabase = false;
                foreach (XmlNode nd in nodes)
                {
                    IBaseUltimateSportsDataInterface sportsDataComponent = new SportsDataComponent();
                    if (checkDatabase)
                    {
                        sportsDataComponent.DatabasePopulated = true;
                    }
                    else
                    {
                        checkDatabase = sportsDataComponent.DatabasePopulated;
                    }

                    sportsDataComponent.Configure(nd);

                    _logger.Debug("Adding " + sportsDataComponent.InstanceName);
                    JobInstances.Add(sportsDataComponent.InstanceName, sportsDataComponent);
                }
            }

        }





        /// <summary>
        /// Starts the main service instance.
        /// </summary>
        /// <remarks>
        /// The function will set up polling and events to Start all processing jobs.
        /// </remarks>
        public void Start()
        {
            ThreadContext.Stacks["NDC"].Push("Start");

            try
            {
                // Start instances
                _logger.Debug("Number of jobs to start: " + JobInstances.Count);

                // Looping jobs
                foreach (
                    KeyValuePair<string, IBaseUltimateSportsDataInterface> kvp in JobInstances.Where(kvp => kvp.Value.ComponentEnabled))
                {
                    _logger.Info("Starting " + kvp.Value.InstanceName);
                    kvp.Value.Start();
                }

                // Starting maintenance
                _logger.Info("Starting maintenance timer");
                maintenanceTimer.Start();

                // Watch the config file
                configFileWatcher.EnableRaisingEvents = WatchConfigSet;
            }
            catch (FaultException fex)
            {
                _logger.Fatal("An error has occured. Could not Start service", fex);
                if (fex.InnerException == null) return;

                _logger.Fatal(fex.InnerException.Message);
                _logger.Fatal(fex.InnerException.StackTrace);
                // Mail.SendException(fex);
            }
            catch (Exception ex)
            {
                _logger.Fatal("An error has occured. Could not Start service", ex);
                _logger.Fatal(ex.StackTrace);
                // Mail.SendException(ex);
            }
            finally
            {
                ThreadContext.Stacks["NDC"].Pop();
            }
        }

        /// <summary>
        /// The pause.
        /// </summary>
        public void Pause()
        {
            maintenanceTimer.Stop();
            configFileWatcher.EnableRaisingEvents = false;

            // Stop instances
            foreach (KeyValuePair<string, IBaseUltimateSportsDataInterface> kvp in JobInstances.Where(kvp => kvp.Value.ComponentEnabled))
            {
                _logger.Info("Stopping " + kvp.Value.InstanceName);
                kvp.Value.Stop();
            }
        }

        /// <summary>
        /// The stop.
        /// </summary>
        public void Stop()
        {
            Pause();

            // Kill notification service handler
            if (ServiceHost == null)
            {
                return;
            }

            ServiceHost.Close();
            ServiceHost = null;
        }

        private void maintenanceTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            MDC.Set("JOBNAME", "MainWorkerJob");

            _logger.Debug("In maintenanceTimer_Elapsed");

            /*
             * Not sure what this shall do yet, 
             * but I would believe that getting some data would be an idea...
             */
            try
            {
                maintenanceTimer.Stop();

                // We are checking if the jobs are doing what they are supposed to do
                foreach (KeyValuePair<string, IBaseUltimateSportsDataInterface> foo in JobInstances.Where(kvp => kvp.Value.ComponentEnabled))
                {
                    _logger.Debug("ComponentState: " + foo.Value.ComponentState.ToString());
                }

                foreach (KeyValuePair<string, IBaseUltimateSportsDataInterface> kvp in

                    JobInstances.Where(kvp => kvp.Value.ComponentEnabled)
                                .Where(kvp => kvp.Value.ComponentState == ComponentState.Halted))
                {
                    _logger.Info("The component is in Halted state, even though it is Enabled");
                    _logger.Debug("We are starting the component!");

                    kvp.Value.Start();
                }

                maintenanceTimer.Start();

                // Watch the config file
                if (!configFileWatcher.EnableRaisingEvents)
                {
                    configFileWatcher.EnableRaisingEvents = WatchConfigSet;
                }
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat(
                    "MainServiceComponent::maintenanceTimer_Elapsed() failed to renew PushSubscription: " + ex.Message,
                    ex);
                Mail.SendException(ex);
            }
        }

        private void configFileWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = "MainServiceWorker";

            // Outer-try-final to prevent dupe events
            try
            {
                // Stopping file watcher
                configFileWatcher.EnableRaisingEvents = false;
                _logger.Info("MainServiceComponent::configFileWatcher_Changed() hit. Will reconfigure.");

                ThreadContext.Stacks["NDC"].Push("RECONFIGURE");

                // Pause everything
                Pause();

                // Give it a little break
                Thread.Sleep(5000);

                // Reconfigure
                ConfigurationManager.RefreshSection("applicationSettings");
                ConfigurationManager.RefreshSection("appSection");
                Configure();

                Start();
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTBSportsData reconfiguration failed - TERMINATING!", ex);
                Mail.SendException(ex);
                throw;
            }
            finally
            {
                ThreadContext.Stacks["NDC"].Pop();
                configFileWatcher.EnableRaisingEvents = WatchConfigSet;
            }
        }

    }
}
