﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml;
using NTB.UltimateSportsData.Classes.Classes;
using Quartz;
using log4net;
using Quartz.Xml.JobSchedulingData20;

namespace NTB.UltimateSportsData.Components.QuartzJobs
{
    public class SportsDataJob : IJob
    {
        /// <summary>
        /// Static logger
        /// </summary>
        static readonly ILog Logger = LogManager.GetLogger(typeof(SportsDataJob));

        static SportsDataJob()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();
        }

        public void Execute(IJobExecutionContext context)
        {
            Logger.Info("Executing JobExecutionContext" + context.JobDetail.Description);

            JobDataMap jobDataMap = context.JobDetail.JobDataMap;
            string apiKey = jobDataMap.GetString("ApiKey");
            string apiUser = jobDataMap.GetString("ApiUser");
            string apiToken = jobDataMap.GetString("ApiToken");
            string apiUrl = jobDataMap.GetString("ApiUrl");
            int eventId = jobDataMap.GetInt("EventId");
            string eventName = jobDataMap.GetString("EventName");
            int records = jobDataMap.GetInt("Records");
            int distance = jobDataMap.GetInt("Distance");
            string filename = jobDataMap.GetString("Filename");
            string fileExtention = jobDataMap.GetString("FileExtention");
            string outputFolders = jobDataMap.GetString("OutputFolders");

            
            var api = new Api
            {
                Key = apiKey,
                Token = apiToken,
                User = apiUser,
                Url = apiUrl
            };

            Logger.Info("Creating ApiClient object");
            var apiClient = new ApiClients.ApiClient(api);

            Logger.InfoFormat("Calling GetResult for event {0}", eventId);
            string resultBody = apiClient.GetResult(eventId, records, distance);

            if (resultBody == string.Empty)
            {
                Logger.Info("The string is empty, so really nothing to do here...");
                return;
            }

            Logger.Info("UTF-8 decode the results");
            resultBody = Utilities.Utilities.Utf8Decode(resultBody);

            Logger.Info("Creating FileCleaner object");
            var cleaner = new FileCleaner();
            resultBody = cleaner.CleanFile(resultBody);

            Logger.Info("Creating XmlCreator object");
            var xmlCreator = new XmlCreator();

            XmlDocument xmlDoc = xmlCreator.ReadDocument(resultBody);
            xmlDoc = xmlCreator.AddXmlElement(xmlDoc, "DistanceName", eventName, "distance", distance.ToString());

            
            Logger.Info("Creating FileCreator object");
            FileCreator creator = new FileCreator();

            Logger.InfoFormat("Calling CreateFile with parameters {0}, {1}, {2}", xmlDoc.ToString().Length, outputFolders, filename + "." + fileExtention);
            creator.CreateFile(xmlDoc, outputFolders, filename + "." + fileExtention);
            
        }

        
        
    }
}
