﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using log4net;
using NTB.UltimateSportsData.Classes.Classes;

namespace NTB.UltimateSportsData.Components
{
    class XmlCreator
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(XmlCreator));

        public XmlCreator()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();
        }

        public XmlDocument ReadDocument(string content)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(content);

            return xmlDoc;
        }

        public XmlDocument AddXmlElement(XmlDocument xmlDoc, string elementName, string elementContent, string attributeName, string attributeValue)
        {
            /*
             * <XML>
    <Data>
        <Info>
            <Records>70</Records>
            <RequestDateTime>18-06-2014 09:18:05</RequestDateTime>
            <Server>live.ultimate.dk @ resultsdb1</Server>
            <ProcessingTime>0.1187</ProcessingTime>
            <Event>Styrkeprøven 2013</Event>
            <Method>lateststandings</Method>
            <APICacheKey>0123456789</APICacheKey>
        </Info>
             */
            if (elementName.Trim() == string.Empty)
            {
                return xmlDoc;
            }
            XmlElement xmlNtbElement = xmlDoc.CreateElement("MetaData");
            XmlElement xmlElement = xmlDoc.CreateElement(elementName);
            if (attributeName.Trim() != string.Empty)
            {
                xmlElement.SetAttribute(attributeName, attributeValue);
            }
            XmlText xmlText = xmlDoc.CreateTextNode(elementContent);
            xmlElement.AppendChild(xmlText);
            xmlNtbElement.AppendChild(xmlElement);
            var dataNode = xmlDoc.SelectSingleNode("//Data");
            if (dataNode != null) dataNode.AppendChild(xmlNtbElement);
            // xmlDoc.AppendChild(xmlNtbElement);

            return xmlDoc;
        }
    }
}
