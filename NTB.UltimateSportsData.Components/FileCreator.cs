﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using log4net;

namespace NTB.UltimateSportsData.Components
{
    public class FileCreator
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(FileCreator));

        public FileCreator()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();
        }

        public void CreateFile(XmlDocument xmlDocument, string path, string filename)
        {
            try
            {
                filename = CreateFileName(filename);
                string outputFile = path + @"\" + filename;
                // this is most likely where we store the file content
                // File.WriteAllText(outputFile, content);

                XmlWriterSettings settings = new XmlWriterSettings {Indent = true, NewLineOnAttributes = false};
                using (var writer = XmlWriter.Create(outputFile, settings))
                {
                    xmlDocument.Save(writer);
                }

            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    Logger.Error(exception.InnerException.Message);
                    Logger.Error(exception.InnerException.StackTrace);
                }
            }
        }

        private string CreateFileName(string filename)
        {
            DateTime dt = DateTime.Now;

            string year = dt.Year.ToString().PadLeft(2, '0');
            string month = dt.Month.ToString().PadLeft(2, '0');
            string day = dt.Day.ToString().PadLeft(2, '0');

            // Putting the filename together
            string hour = dt.Hour.ToString().PadLeft(2, '0');
            string minutes = dt.Minute.ToString().PadLeft(2, '0');
            string seconds = dt.Second.ToString().PadLeft(2, '0');

            filename = filename.Replace("[%Y]", year)
                               .Replace("[%M]", month)
                               .Replace("[%D]", day)
                               .Replace("[%H]", hour)
                               .Replace("[%m]", minutes)
                               .Replace("[%S]", seconds);
            return Regex.Replace(filename, @"\[\%\[A-Za-z]\]", "");

        }
    }
}
