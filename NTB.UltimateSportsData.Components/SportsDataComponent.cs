﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using NTB.UltimateSportsData.Classes.Classes;
using NTB.UltimateSportsData.Classes.Enums;
using NTB.UltimateSportsData.Components.Exceptions;
using NTB.UltimateSportsData.Components.Interfaces;

// Logging
using log4net;
using NTB.UltimateSportsData.Components.QuartzJobs;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;

namespace NTB.UltimateSportsData.Components
{
    public partial class SportsDataComponent : Component, IBaseUltimateSportsDataInterface
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SportsDataComponent));
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        public SportsDataComponent()
        {
            InitializeComponent();
        }

        public SportsDataComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        /// <summary>
        /// The configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected string Name;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly configured
        /// </summary>
        /// <remarks>Holds the configured status of the instance. <c>True</c> means successfully configured</remarks>
        protected bool Configured = false;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected bool ErrorRetry = false;

        /// <summary>
        /// PollDelay is used to make sure that the file is ready to be accessed. 
        /// </summary>
        protected int PollDelay = 0;

        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected string EmailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailSubject;

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailBody;

        /// <summary>
        /// The configured schedule for this instance
        /// </summary>
        /// <remarks>This is set to a time of day and is used by the <c>Scheduled</c> pollstyle. Work is triggered at the given time of day.</remarks>
        protected string Schedule;

        /// <summary>
        /// The configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;

        /// <summary>
        /// The enabled status for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="ComponentEnabled"/></remarks>
        protected Boolean enabled;

        /// <summary>
        /// Flag to tell which state the component is in (running/halted)
        /// </summary>
        protected ComponentState componentState;

        /// <summary>
        /// Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private readonly AutoResetEvent _stopEvent = new AutoResetEvent(false);

        /// <summary>
        /// Get or set name of file to be produced
        /// </summary>
        private string Filename { get; set; }

        /// <summary>
        /// Get or set the extention of the file to be produced
        /// </summary>
        private string Extention { get; set; }

        /// <summary>
        /// schedulerFactory to be used to create scheduled tasks
        /// </summary>
        protected StdSchedulerFactory SchedulerFactory = new StdSchedulerFactory();

        /// <summary>
        /// The scheduler.
        /// </summary>
        protected IScheduler Scheduler;

        /// <summary>
        /// List of schedules.
        /// </summary>
        protected List<Schedule> DownloadSchedules { get; set; }

        public List<SportEvent> ComponentSportEvents { get; set; }

        public Api ComponentApi { get; set; }

        /// <summary>
        /// The polling style for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="PollStyle"/></remarks>
        protected PollStyle pollStyle;

        public bool ComponentEnabled
        {
            get { return enabled; }
            set { enabled = value; }
        }
        public bool DatabasePopulated { get; set; }
        public string InstanceName { get; set; }
        public OperationMode OperationMode { get; private set; }
        
        public ComponentState ComponentState { get; private set; }
        public MaintenanceMode MaintenanceMode { get; private set; }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Converter</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get
            {
                return pollStyle;
            }
        }

        /// <summary>
        /// Output file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter"/> and <see cref="IncludeSubdirs"/>
        /// </remarks>
        protected Dictionary<int, string> FileOutputFolders = new Dictionary<int, string>();


        public void Configure(XmlNode configNode)
        {
            Logger.Debug("Node: " + configNode.Name);

            // Basic configuration sanity check
            if (configNode.Attributes != null
                && (configNode.Name != "SportsDataComponent" || configNode.Attributes.GetNamedItem("Name") == null))
            {
                throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
            }

            // Getting the name of this Component instance
            try
            {
                if (configNode.Attributes != null)
                {
                    InstanceName = configNode.Attributes["Name"].Value;

                    ThreadContext.Stacks["NDC"].Push(InstanceName);

                    ComponentEnabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance. Check error log for more information.");
                ErrorLogger.Fatal("Not possible to configure this job instance!", ex);

                ThreadContext.Stacks["NDC"].Pop();

                throw new ArgumentException("Invalid or missing values in XML configuration node", ex);
            }

            // Basic operation
            try
            {
                if (configNode.Attributes != null)
                {
                    pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
            }

            SetOutputFolders(configNode);

            SetApiKey(configNode);

            SetEventInformation(configNode);

            if (enabled)
            {
                try
                {
                    // Setting up polling
                    switch (PollStyle)
                    {
                        case PollStyle.Continous:
                            SetContinousPollStyle(configNode);
                            break;

                        case PollStyle.CalendarPoll:
                            SetCalendarPollStyle(configNode);
                            break;

                        case PollStyle.Scheduled:
                            SetScheduledPollStyle(configNode);
                            break;

                        default:

                            // Unsupported pollstyle for this object
                            throw new NotSupportedException("Invalid polling style for this job type");
                    }
                }
                catch (Exception exception)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException(
                        "Invalid or missing PollStyle-specific values in XML configuration node: " + exception.Message, exception);
                }
            }

            // Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            Configured = true;
        }

        private void SetSchedule(XmlNode configNode)
        {
            try
            {
                XmlNodeList nodes = configNode.SelectNodes("Schedules/Schedule");

                if (nodes == null) return;


                var schedules = new List<Schedule>();

                foreach (XmlNode node in nodes)
                {
                    if (node == null || node.Attributes == null) continue;
                    var schedule = new Schedule();

                    if (node.Attributes["Date"] != null)
                    {
                        schedule.Date = Convert.ToDateTime(node.Attributes["Date"].Value);
                    }

                    if (node.Attributes["Time"] != null)
                    {
                        schedule.Time = Convert.ToInt32(node.Attributes["Time"].Value.Replace(":", string.Empty));
                    }

                    schedule.GetResults = false;
                    if (node.Attributes["Results"] != null)
                    {
                        schedule.GetResults = Convert.ToBoolean(node.Attributes["Results"].Value);
                    }

                    if (node.Attributes["Interval"] != null)
                    {
                        schedule.Interval = Convert.ToInt32(node.Attributes["Interval"].Value);
                    }

                    if (node.Attributes["Timespan"] != null)
                    {
                        schedule.TimeSpan= Convert.ToInt32(node.Attributes["Timespan"].Value);
                    }

                    if (node.Attributes["Id"] != null)
                    {
                        schedule.Id = node.Attributes["Id"].Value;
                    }
                    else
                    {
                        schedule.Id = schedule.Date.ToString().Replace("-", string.Empty) + schedule.Time;
                    }

                    schedules.Add(schedule);
                }

                DownloadSchedules = schedules;
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get Schedule Information. Check error log for more information.");
                ErrorLogger.Fatal("Not possible to get Schedule Information", exception);
            }
        }

        private void SetCalendarPollStyle(XmlNode configNode)
        {
            // Get the jobs
            // SetSchedule(configNode);
            
            Scheduler = SchedulerFactory.GetScheduler();

            

            if (ComponentSportEvents.Count > 0)
            {
                foreach (SportEvent sportevent in ComponentSportEvents)
                {
            
                    // TODO: Look over this 
                    IJobDetail job = JobBuilder.Create<SportsDataJob>()
                        .WithIdentity("USDCalendar_" + InstanceName + "_" + sportevent.EventName)
                        .Build();

                    job.JobDataMap["ApiKey"] = ComponentApi.Key;
                    job.JobDataMap["ApiUser"] = ComponentApi.User;
                    job.JobDataMap["ApiToken"] = ComponentApi.Token;
                    job.JobDataMap["ApiUrl"] = ComponentApi.Url;
                    job.JobDataMap["EventId"] = sportevent.EventId;
                    job.JobDataMap["EventName"] = sportevent.EventName;
                    job.JobDataMap["Records"] = sportevent.Records;
                    job.JobDataMap["Distance"] = sportevent.Distance;
                    job.JobDataMap["OutputFolders"] = Utilities.Utilities.DictionaryValuesToString(FileOutputFolders);
                    job.JobDataMap["Filename"] = sportevent.EventFile.Name;
                    job.JobDataMap["FileExtention"] = sportevent.EventFile.Extention;

                    int timespan = sportevent.TimeSpan;

                    if (timespan == 0)
                    {
                        timespan = 3;
                    }

                    int interval = sportevent.Interval;
                    if (interval == 0)
                    {
                        interval = 15;
                    }

                    
                    // This one will go daily since we don't have date information. 
                    // the trigger will run every 30 minutes for 12 hours.
                    ITrigger trigger = TriggerBuilder.Create()
                        .WithIdentity("USDTrigger_" + sportevent.EventName.Replace(" ", string.Empty) + sportevent.EventId, "DEFAULT")
                        .ForJob(job.Key)
                        .StartAt(sportevent.EventDateStart.ToUniversalTime())
                        .WithSimpleSchedule(x => x.WithIntervalInMinutes(interval)
                            .RepeatForever())
                        .EndAt(sportevent.EventDateEnd.AddHours(timespan).ToUniversalTime())
                        .Build();

                    Scheduler.ScheduleJob(job, trigger);

                    Scheduler.Start();
                    
                }
            }
            //ITrigger trigger = TriggerBuilder.Create()
            //    .WithIdentity("USDTrigger_" + InstanceName, "UltimateSportsDataService")
            //    .ForJob("USDJob_" + InstanceName)
            //    .StartAt(ComponentSportEvent.EventDateStart)
            //    .WithSimpleSchedule(x => x.WithIntervalInMinutes(2)
            //        .RepeatForever())
            //    .EndAt(DateBuilder.DateOf(23, 0, 0, ComponentSportEvent.EventDateEnd.Day, ComponentSportEvent.EventDateEnd.Month, ComponentSportEvent.EventDateEnd.Year))
            //    .Build();


        }

        private void SetScheduledPollStyle(XmlNode configNode)
        {
            
        }

        private void SetContinousPollStyle(XmlNode configNode)
        {
            if (configNode.Attributes != null)
            {
                Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
            }

            if (Interval == 0)
            {
                Interval = 5000;
            }

            pollTimer.Interval = Interval*1000;
            Logger.DebugFormat("Poll interval: {0} seconds", Interval);
        }

        

        private string CreateToken(string apikey)
        {
            // md5 (“[your API secret]” + md5 (“API@UltimateLIVE"))
            // 3a87368db8ae28c6662c8fc5251a07bf
            const string salt = "API@UltimateLIVE"; // 8a417ffa23d1159f8c8086b976fab10c
            MD5 md5 = MD5.Create();

            UTF8Encoding encoding = new UTF8Encoding();

            byte[] hashSalt = md5.ComputeHash(encoding.GetBytes(salt));
            
            string hashedSalt = BitConverter.ToString(hashSalt).Replace("-", "").ToLower();

            // Now we shall encrypt the whole shebang

            string toSignData = apikey + hashedSalt;

            byte[] hash = md5.ComputeHash(encoding.GetBytes(toSignData));

            string hashedString = BitConverter.ToString(hash).Replace("-", "").ToLower();

            return hashedString; // "9ee263556abc74fddfde51465a52242b" -> 9ee263556abc74fddfde51465a52242b

        }

        private void SetApiKey(XmlNode configNode)
        {
            try
            {
                XmlNode node = configNode.SelectSingleNode("Api");

                if (node == null || node.Attributes == null) return;


                var api = new Api();

                if (node.Attributes["key"] != null)
                {
                    api.Key = node.Attributes["key"].Value;

                    api.Token = CreateToken(api.Key);
                }

                if (node.Attributes["user"] != null)
                {
                    api.User = node.Attributes["user"].Value;
                }

                if (node.Attributes["url"] != null)
                {
                    api.Url = node.Attributes["url"].Value;
                }

                ComponentApi = api;
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get Api Information. Check error log for more information.");
                ErrorLogger.Fatal("Not possible to get Api Information", exception);
            }
        }

        private string CreateDateString(string dateString)
        {
            string[] dateArray = dateString.Split('-');
            return dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0];
        }
        private void SetEventInformation(XmlNode configNode)
        {
            try
            {
                XmlNodeList nodes = configNode.SelectNodes("Events/Event");
                List<SportEvent> sportEvents = new List<SportEvent>();

                if (nodes != null)
                {
                    foreach (XmlNode node in nodes)
                    {
                        var sportEvent = new SportEvent();
                        if (node == null || node.Attributes == null) continue;
                        
                        if (node.Attributes["id"] != null)
                        {
                            sportEvent.EventId = Convert.ToInt32(node.Attributes["id"].Value);
                        }

                        if (node.Attributes["startdatetime"] != null)
                        {
                            sportEvent.EventDateStart = Convert.ToDateTime(node.Attributes["startdatetime"].Value);
                        }

                        if (node.Attributes["enddatetime"] != null)
                        {
                            sportEvent.EventDateEnd = Convert.ToDateTime(node.Attributes["enddatetime"].Value);
                        }

                        if (node.Attributes["interval"] != null)
                        {
                            sportEvent.Interval = Convert.ToInt32(node.Attributes["interval"].Value);
                        }

                        if (node.Attributes["timespan"] != null)
                        {
                            sportEvent.TimeSpan = Convert.ToInt32(node.Attributes["timespan"].Value);
                        }

                        if (node.Attributes["records"] != null)
                        {
                            sportEvent.Records = Convert.ToInt32(node.Attributes["records"].Value); 
                        }

                        if (node.Attributes["distance"] != null)
                        {
                            sportEvent.Distance = Convert.ToInt32(node.Attributes["distance"].Value);
                        }

                        if (node.Attributes["name"] != null)
                        {
                            sportEvent.EventName= node.Attributes["name"].Value;
                        }

                        var fileNode = node.SelectSingleNode("Filename");
                        
                        var eventFile = new EventFile();
                        
                        if (fileNode != null)
                        {
                            if (fileNode.Attributes != null)
                            {
                                if (fileNode.Attributes["name"] != null)
                                {
                                    eventFile.Name = fileNode.Attributes["name"].Value;
                                }

                                if (fileNode.Attributes["extention"] != null)
                                {
                                    eventFile.Extention = fileNode.Attributes["extention"].Value;
                                }
                            }
                        }
                        sportEvent.EventFile = eventFile;

                        sportEvents.Add(sportEvent);
                    }
                }

                ComponentSportEvents = sportEvents;
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get Event Information. Check error log for more information.");
                ErrorLogger.Fatal("Not possible to get Event Information", exception);
            }
        }

        private void CreateOutputFolders(string path)
        {
            DirectoryInfo di = new DirectoryInfo(path);

            if (!di.Exists)
            {
                di.Create();
            }
        }

        

        private void SetOutputFolders(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                XmlNodeList folderNodes = configNode.SelectNodes("Folders/Folder");
                if (folderNodes != null)
                {
                    foreach (XmlNode folderNode in folderNodes.Cast<XmlNode>().Where(folderNode => folderNode != null))
                    {
                        if (folderNode.Attributes == null)
                        {
                            continue;
                        }
                        
                        CreateOutputFolders(folderNode.InnerText);
                        
                        FileOutputFolders.Add(Convert.ToInt32(folderNode.Attributes["Id"].Value), folderNode.InnerText);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder. Check error log for more information.");
                ErrorLogger.Fatal("Not possible to get output-folder", ex);
            }
        }

        public void Start()
        {
            if (!Configured)
            {
                throw new SportsDataException("SportsDataComponent for " + InstanceName + " is not properly Configured. SportsDataComponent::Start() Aborted!");
            }

            if (!enabled)
            {
                throw new SportsDataException("SportsDataComponent for " + InstanceName + " is not Enabled. SportsDataComponent::Start() Aborted!");
            }

            if (pollStyle == PollStyle.Continous)
            {
                Logger.Info("Starting Continous PollStyle");
                pollTimer.Enabled = true;
            }

            if (pollStyle == PollStyle.CalendarPoll)
            {
                Logger.Info("Setting up job listener and job scheduler");

                // Starting the scheduler
                var schedSportsData = SchedulerFactory.GetScheduler();

                IList<string> jobGroupNamesList = schedSportsData.GetJobGroupNames();

                foreach (string jobGroupName in jobGroupNamesList)
                {
                    Logger.Info("Setting up Job Group with name :" + jobGroupName);

                    var groupMatcher = GroupMatcher<JobKey>.GroupContains(jobGroupName);

                    var jobKeys = schedSportsData.GetJobKeys(groupMatcher);
                }

                IList<string> triggerGroupNames = schedSportsData.GetTriggerGroupNames();

                schedSportsData.Start();
            }

            _stopEvent.Reset();
            _busyEvent.Set();

            // Starting poll-timer
            // pollTimer.Interval = 2000;

            // Setting ComponentState to running
            ComponentState = ComponentState.Running;

            pollTimer.Start();
            // throw new NotImplementedException();
        }

        public void Stop()
        {
            if (!Configured)
            {
                throw new SportsDataException("SportsDataComponent for " + InstanceName + " is not properly Configured. SportsDataComponent::Start() Aborted!");
            }

            if (!enabled)
            {
                throw new SportsDataException("SportsDataComponent for " + InstanceName + " is not Enabled. SportsDataComponent::Start() Aborted!");
            }

            if (pollStyle == PollStyle.CalendarPoll)
            {
                try
                {
                    Logger.Info("Deleting / stopping jobs!");
                    ISchedulerFactory sf = new StdSchedulerFactory();
                    IScheduler sched = sf.GetScheduler();

                    // Shutting down scheduler
                    sched.GetJobGroupNames();

                    List<JobKey> keys = new
                        List<JobKey>(sched.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(InstanceName)));

                    // Looping through each key as we'd like to know when things go right and not!
                    keys.ForEach(key =>
                    {
                        IJobDetail detail = sched.GetJobDetail(key);
                        sched.DeleteJob(key);

                        Logger.Info("Shutting down: " + detail.Description + "(" + key.Name + ")" + " Group: " +
                                    key.Group);
                    });

                    sched.Shutdown();
                }
                catch (SchedulerException sex)
                {
                    Logger.Fatal(sex);
                }
                catch (Exception ex)
                {
                    Logger.Error("Problems closing SportsData Components!", ex);
                }

                const double epsilon = 0;
                
                if (!pollTimer.Enabled && (
                    ErrorRetry || pollStyle != PollStyle.FileSystemWatch || 
                    Math.Abs(pollTimer.Interval - 5000) < epsilon))
                {
                    Logger.InfoFormat("Waiting for instance to complete work. Job {0}", InstanceName);
                }

                // Signal Stop
                _stopEvent.Set();

                
                ComponentState = ComponentState.Halted;

                // Kill polling
                pollTimer.Stop();

            }
        }
    }
}
