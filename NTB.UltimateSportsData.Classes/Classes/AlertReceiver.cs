﻿using NTB.UltimateSportsData.Classes.Enums;

namespace NTB.UltimateSportsData.Classes.Classes
{
    public class AlertReceiver
    {
        public AlertLevel AlertLevel { get; set; }

        public string Receiver { get; set; }
    }
}
