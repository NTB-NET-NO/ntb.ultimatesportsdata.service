﻿using System;
using System.Collections.Generic;

namespace NTB.UltimateSportsData.Classes.Classes
{
    public class Match
    {
        public int Id { get; set; }
        
        public int ActivityId { get; set; }
        
        public string ClassCodeNo { get; set; }
        
        public string ClassCodeName { get; set; }
        
        public string MatchNo { get; set; }
        
        public DateTime MatchDate { get; set; }
        
        public int MatchEndTime { get; set; }
        
        public int MatchStartTime { get; set; }
        
        public int MatchTypeId { get; set; }
        
        public int RoundId { get; set; }
        
        public string RoundName { get; set; }
        
        public int TournamentId { get; set; }
        
        public int VenueUnitId { get; set; }

        public int HomeTeamId { get; set; }
        
        public string HomeTeamName { get; set; }
        
        public int HomeTeamGoals { get; set; }
        
        public int AwayTeamId { get; set; }
        
        public string AwayTeamName { get; set; }
        
        public int AwayTeamGoals { get; set; }

        public int SportId { get; set; }

        public bool Downloaded { get; set; }

        public string PartialResult { get; set; }

        public string MatchResult { get; set; }

        public string ResultFormatted { get; set; }

        public List<Player> HomeTeamPlayers { get; set; }

        public List<Player> AwayTeamPlayers { get; set; }

        public List<Player> MatchPlayers { get; set; }

        public List<MatchIncident> MatchIncidents { get; set; }

        public List<Referee> Referees { get; set; }

        public string StatusCode { get; set; }
    }
}
