﻿namespace NTB.UltimateSportsData.Classes.Classes
{
    public class OutputFolder
    {
        public string OutputPath { get; set; }

        public int? DisciplineId { get; set; }

        public int OrgId { get; set; }

        public int SportId { get; set; }
    }
}
