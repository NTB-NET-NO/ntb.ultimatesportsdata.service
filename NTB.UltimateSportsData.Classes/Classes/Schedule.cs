﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.UltimateSportsData.Classes.Classes
{
    public class Schedule
    {
        public string Id { get; set; }
        public DateTime? Date { get; set; }
        public int Time { get; set; }
        public bool GetResults { get; set; }
        public int Interval { get; set; }
        public int TimeSpan { get; set; }

    }
}
