﻿namespace NTB.UltimateSportsData.Classes.Classes
{
    public class Referee
    {
        public int Id { get; set; }
        public string RefereeName { get; set; }
        public int RefereeTypeId { get; set; }
        public string RefereeType { get; set; }
        public int ClubId { get; set; }
        public string ClubName { get; set;}
    }
}
