namespace NTB.UltimateSportsData.Classes.Classes
{
    public class Club
    {
        public int ClubId { get; set; }
        public string Name { get; set; }
    }
}
