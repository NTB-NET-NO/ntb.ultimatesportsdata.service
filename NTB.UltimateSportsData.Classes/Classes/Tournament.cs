﻿
namespace NTB.UltimateSportsData.Classes.Classes
{
    public class Tournament
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int SportId { get; set; }

        public int Number { get; set; }

        public int SeasonId { get; set; }

        public int TypeId { get; set; }
        
        public int DistrictId { get; set; }

        public int AgeCategoryId { get; set; }

        public int GenderId { get; set; }

        public string StatusCode { get; set; }

        public bool NationalTeamTournament { get; set; }

        public int Division { get; set; }
    }
}
