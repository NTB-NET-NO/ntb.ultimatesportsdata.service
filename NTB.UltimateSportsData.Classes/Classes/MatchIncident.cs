﻿namespace NTB.UltimateSportsData.Classes.Classes
{
    public class MatchIncident
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string IncidentShort { get; set; }

        public string IncidentType { get; set; }

        public int IncidentTypeId { get; set; }

        public int MatchId { get; set; }

        public int Period { get; set; }

        public string Reason { get; set; }

        public int Time { get; set; }

        public int Value { get; set; }

        public int TeamId { get; set; }

        public Player Player { get; set; }
    }
}
