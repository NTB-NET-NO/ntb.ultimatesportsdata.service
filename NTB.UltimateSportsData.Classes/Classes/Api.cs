﻿namespace NTB.UltimateSportsData.Classes.Classes
{
    public class Api
    {
        public string Key { get; set; }

        public string User { get; set; }

        public string Token { get; set; }

        public string Url { get; set; }

    }
}
