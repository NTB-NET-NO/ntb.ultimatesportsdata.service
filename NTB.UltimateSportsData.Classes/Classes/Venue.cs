﻿namespace NTB.UltimateSportsData.Classes.Classes
{
    public class Venue
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int LocalCouncilId { get; set; }

        public string LocalCouncilName { get; set; }

        public string Place { get; set; }
    }
}
