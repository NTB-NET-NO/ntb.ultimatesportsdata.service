﻿namespace NTB.UltimateSportsData.Classes.Classes
{
    public class Organization
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int SportId { get; set; }

        public string NameShort { get; set; }

        public int SingleSport { get; set; }

        public int TeamSport { get; set; }

        public string Sport { get; set; }
    }
}
