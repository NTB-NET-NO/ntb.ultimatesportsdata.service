﻿using System;
using NTB.UltimateSportsData.Classes.Enums;

namespace NTB.UltimateSportsData.Classes.Classes
{
    public class Document
    {
        public string Filename { get; set; }
        
        public int Version { get; set; }
        
        public DateTime Created { get; set; }
        
        public int TournamentId { get; set; }
        
        public int SportId { get; set; }

        public string FullName { get; set; }

        public DocumentType DocumentType { get; set; }
    }
}
