namespace NTB.UltimateSportsData.Classes.Classes
{
    public class Athlete
    {
        public int PersonId { get; set; }
        public int CompetitorId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
        
        public string Gender { get; set; }
        
        public string Nationality { get; set; }

        public int ClubId { get; set; }

        public string ClubName { get; set; }

        public int SportEventId { get; set; }

    }
}
