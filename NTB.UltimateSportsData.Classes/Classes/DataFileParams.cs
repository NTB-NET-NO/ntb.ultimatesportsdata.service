﻿using System;
using System.Collections.Generic;

namespace NTB.UltimateSportsData.Classes.Classes
{
    public class DataFileParams
    {
        public string FileOutputFolder { get; set; }

        public bool PopulateDatabase { get; set; }

        public string ScheduleType { get; set; }

        public bool Results { get; set; }

        public bool ScheduleMessage { get; set; }

        public int Duration { get; set; }

        public bool Purge { get; set; }

        public bool CreateXml { get; set; }

        public bool DateOffset { get; set; }

        public int SportId { get; set; }

        public int FederationId { get; set; }

        public DateTime DateStart { get; set; }

        public DateTime DateEnd { get; set; }

        public bool PushResults { get; set; }

        public bool PushSchedule { get; set; }

        public int SeasonId { get; set; }

        public int DisciplineId { get; set; }

        public Enums.PollStyle PollStyle { get; set; }

        public Enums.OperationMode OperationMode { get; set; }

        public List<OutputFolder> FileOutputFolders { get; set; }


        public List<AlertReceiver> AlertReceivers { get; set; }
    }
}
