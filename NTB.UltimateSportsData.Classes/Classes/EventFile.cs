namespace NTB.UltimateSportsData.Classes.Classes
{
    public class EventFile
    {
        /// <summary>
        /// Gets or sets the name of the file
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the extention for the file
        /// </summary>
        public string Extention { get; set; }
    }
}