// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SportEvent.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The sport event.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace NTB.UltimateSportsData.Classes.Classes
{
    /// <summary>
    /// The sport event.
    /// </summary>
    public class SportEvent
    {
        /// <summary>
        /// Gets or sets the event id.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Gets or sets the event organizer id.
        /// </summary>
        public int EventOrganizerId { get; set; }

        /// <summary>
        /// Gets or sets the event name.
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// Gets or sets the event organization name.
        /// </summary>
        public string EventOrganizationName { get; set; }

        /// <summary>
        /// Gets or sets the event date start.
        /// </summary>
        public DateTime EventDateStart { get; set; }

        /// <summary>
        /// Gets or sets the event date end.
        /// </summary>
        public DateTime EventDateEnd { get; set; }

        /// <summary>
        /// Gets or sets the event time start.
        /// </summary>
        public int EventTimeStart { get; set; }

        /// <summary>
        /// Gets or sets the event time end.
        /// </summary>
        public int EventTimeEnd { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of records we are to fetch
        /// </summary>
        public int Distance { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of records we are to fetch
        /// </summary>
        public int Records { get; set; }

        public int Interval { get; set; }

        public int TimeSpan { get; set; }

        public EventFile EventFile { get; set; }


    }
}