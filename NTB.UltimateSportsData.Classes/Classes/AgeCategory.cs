﻿namespace NTB.UltimateSportsData.Classes.Classes
{
    public class AgeCategory
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int FromAge { get; set; }

        public int ToAge { get; set; }

        public int FederationId { get; set; }

        public int SportId { get; set; }
    }
}
