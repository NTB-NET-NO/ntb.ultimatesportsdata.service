﻿namespace NTB.UltimateSportsData.Classes.Enums
{
    public enum AlertLevel
    {
        /// <summary>
        ///     States that the level of alert is error
        /// </summary>
        Error,

        Info,

        Fatal
    }
}
